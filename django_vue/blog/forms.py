# -*- coding: utf-8 -*-
from django import forms

from .models import Post

class PostForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(PostForm, self).__init__(*args, **kwargs)
        self.fields['title'].widget.attrs = {
            'class': 'form-control col-md-6'
        }
        self.fields['body'].widget.attrs = {
            'class': 'form-control col-md-6'
        }

    class Meta:
        model = Post
        fields = ('title', 'body')