from .models import Post
from .serializers import PostSerializer
from django.shortcuts import render
from django.utils import timezone
from django.views.generic import ListView, DetailView, DeleteView, CreateView, UpdateView
from django.contrib.messages.views import SuccessMessageMixin
from .forms import PostForm
from django.urls import reverse_lazy
from rest_framework import generics, status
from rest_framework.response import Response

def vue_view(request):

    context = {
        'posts': Post.objects.all()
    }
    context['vue_element'] = 'vueblog'
    return render(request, 'blog/vue_blog.html', context)

class PostCreate(SuccessMessageMixin, CreateView): 
    model = Post
    form_class = PostForm
    success_url = reverse_lazy('vue_blog')
    success_message = "Post successfully created!"

class PostDetailView(DetailView):

    # context_object_name = 'post' 7/12/2022
    # template_name = 'blog/django_detail.html' jawaid 7/12/2022
    model = Post

class PostListView(ListView):
    
    queryset = Post.objects.all()
    context_object_name = 'post_list'
    template_name = 'blog/django_list.html'

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset(*args, **kwargs)
        qs = qs.filter(published__lte=timezone.now())
        return qs

    class Meta:
        model = Post


class PostDetailAPIView(generics.RetrieveAPIView):
    
    serializer_class = PostSerializer
    queryset = Post.objects.all()


class PostCreateAPIView(generics.CreateAPIView):

    serializer_class = PostSerializer


class PostListAPIView(generics.ListAPIView):

    queryset = Post.objects.all()
    serializer_class = PostSerializer

    def get_queryset(self, *args, **kwargs):
        nowish = timezone.now()
        qs = super().get_queryset(*args, **kwargs)
        qs = qs.filter(published__lte=nowish)
        return qs


class PostDeleteAPIView(generics.DestroyAPIView):

    queryset = Post.objects.all()
    serializer_class = PostSerializer

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response({
            "message":"Post deleted successfully"
        },
        status=status.HTTP_200_OK)

    def perform_destroy(self, instance):
        instance.delete()

    class Meta:
        model = Post


class PostUpdateAPIView(generics.UpdateAPIView):

    serializer_class = PostSerializer
    queryset = Post.objects.all()

class PostUpdate(SuccessMessageMixin, UpdateView): 
    model = Post
    form_class = PostForm
    success_url = reverse_lazy('vue_blog')
    success_message = "Post successfully updated!"

class PostDelete(SuccessMessageMixin, DeleteView):
    model = Post
    success_url = reverse_lazy('vue_blog')
    success_message = "Post successfully deleted!"